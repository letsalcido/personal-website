import React from 'react';
import Button from '../../components/Button';

export default function LongPage() {
    return (
      <>
          <h1>Hipster Ipsum!</h1>
          <p><Button>This is some text</Button></p>
          <p>I'm baby lyft sriracha chartreuse kitsch XOXO, pour-over brooklyn ramps artisan tumblr subway tile whatever letterpress shaman flexitarian. Man braid raclette fingerstache, irony flexitarian lumbersexual listicle marfa waistcoat pug neutra. Waistcoat chia intelligentsia shabby chic sartorial banjo pop-up tbh tattooed vice. Typewriter artisan post-ironic, hexagon lyft edison bulb tumblr fashion axe la croix pour-over cronut migas four loko chartreuse thundercats. Aesthetic pickled ramps mlkshk pitchfork.</p>

          <p>Raw denim raclette leggings 3 wolf moon art party butcher chartreuse selfies lo-fi squid messenger bag jianbing kogi cliche pork belly. Wayfarers man braid mumblecore, paleo snackwave vegan four dollar toast small batch chicharrones cliche knausgaard tumblr selfies. Semiotics kombucha kinfolk, flannel retro fashion axe taiyaki. Fashion axe stumptown photo booth dreamcatcher tote bag hot chicken you probably haven't heard of them leggings gastropub, narwhal selfies freegan jianbing.</p>

          <p>Chia tacos synth, etsy artisan pitchfork next level readymade palo santo fingerstache waistcoat you probably haven't heard of them tbh wolf truffaut. Green juice synth normcore typewriter fam, authentic bicycle rights sriracha 3 wolf moon coloring book chambray brunch intelligentsia. Before they sold out chambray drinking vinegar mixtape keffiyeh, taxidermy slow-carb cardigan mustache waistcoat live-edge portland typewriter echo park. Coloring book humblebrag master cleanse sriracha vinyl deep v af 90's ramps listicle yuccie ethical man bun hoodie bushwick. Art party hammock drinking vinegar scenester, austin kale chips jianbing keytar health goth woke XOXO. Four dollar toast keffiyeh leggings organic, 90's scenester woke brooklyn vape tacos af heirloom helvetica asymmetrical vinyl. Waistcoat godard hoodie, tumblr af pinterest meditation.</p>

          <p>Kinfolk fam pickled, letterpress lyft blue bottle hoodie mustache PBR&B palo santo plaid semiotics iceland. Four loko flannel echo park adaptogen waistcoat locavore cloud bread affogato, pour-over pabst. Tacos asymmetrical photo booth, schlitz hexagon godard sriracha fam synth chia. Banh mi ugh paleo la croix coloring book williamsburg.</p>

           <p>Kombucha succulents fam, brooklyn craft beer small batch salvia. Sartorial vegan tbh cliche. Literally pickled bushwick la croix banjo venmo affogato butcher 8-bit asymmetrical. Plaid cardigan deep v cornhole beard la croix meditation semiotics roof party neutra trust fund. Migas waistcoat pickled stumptown retro vinyl la croix tumblr lo-fi put a bird on it VHS copper mug.</p>
      </>
    );
}