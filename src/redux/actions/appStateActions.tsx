// actions move later
export enum actionTypes {
    TOGGLE_SHOW_BG_ANIMATION = "TOGGLE_SHOW_BG_ANIMATION"
}

export function toggleShowBackgroundAnimation() {
    return {
        type: actionTypes.TOGGLE_SHOW_BG_ANIMATION
    }
}